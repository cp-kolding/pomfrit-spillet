var app = {
    /**
     * Denne funktion står for at sætte vores app op.
     * Lige nu registrere vi 3 "listeners" - kode der lytter på handlinger i appen.
     *
     * deviceready:
     *   Når appen er startet og klar til at der skal ske noget.
     * sleep:
     *   Når man minimere appen, eller sender den i baggrunden af en anden app.
     * resume:
     *   Når appen vågner igen - eller får fokus igen.
     */
    initialize: function() {
        // document.addEventListener er en funktion vi kan bruge til at opstille "listeners" - kode der lytter efter handlinger (events).
        // her bruger vi den til at lytte efter deviceready, sleep og resume events.
        document.addEventListener('deviceready', this.onDeviceStateChange.bind(this, 'ready'), false);
        document.addEventListener('sleep', this.onDeviceStateChange.bind(this, 'sleep'), false);
        document.addEventListener('resume', this.onDeviceStateChange.bind(this, 'resume'), false);
    },

    // Denne funktion modtager kaldet fra vores 3 handlinger ovenfor.
    onDeviceStateChange: function(state) {
        // Vi starter en nedtælling når appen er startet.
        if ('ready' === state) {
            countDown(3);
        }

        // Kommer appen ud af dvale, genstarter vi nedtællingen.
        if ('resume' === state) {
            // document.getElementById er en funktion der kan give os en reference til DOM objektet der har id="hello-world" (se index.html)
            // vi sætter teksten i det element til "- we'r back! -"
            document.getElementById('hello-world').innerText = "- we'r back! -";

            // window.setTimeout er en funktion der kan kalde en anden funktion når der er gået x milisekunder.
            // her kalder vi funktionen "countDown" efter 2 sekunter.
            window.setTimeout(countDown, 2000, 3);
        }
    },
};

// Funktionen her står for nedtællingen fra 3 til 0.
const countDown = function(i) {
    if (i === 0) {
        return document.getElementById('hello-world').innerText = 'Arrrrrrrh!';
    }

    document.getElementById('hello-world').innerText = i;
    window.setTimeout(countDown, 1000, i - 1);
};

app.initialize();
